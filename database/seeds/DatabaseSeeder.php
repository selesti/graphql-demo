<?php

use App\Client;
use App\Person;
use App\Project;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->people();
        $this->clientsAndProjects();
    }

    private function people()
    {
        factory(Person::class, 30)->create();
    }

    private function clientsAndProjects()
    {
        $clients = factory(Client::class, 20)->create();

        $clients->each(function (Client $client) {
            $client->projects()->saveMany(
               factory(Project::class, 5)->make()
            );
        });
    }

}
