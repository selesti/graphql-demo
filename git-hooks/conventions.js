const glob = require('glob-all');
const path = require('path');
const pkg = require(process.env.PWD + '/package.json');

const ignoreList = [];

if (pkg.config && pkg.config.conventions && pkg.config.conventions.enabled === false) {
    console.warn('🧐 Convention checks skipped...');
    process.exit();
}

if (pkg.config && pkg.config.conventions && pkg.config.conventions.ignore) {
    ignoreList.push(...pkg.config.conventions.ignore);
}

const rules = {
    'no spaces': new RegExp('[ ]'),
    'no capitals': new RegExp('[A-Z]'),
    'no underscores': new RegExp('[_]'),
    'no hyphen': new RegExp('[-]'),
    'has leading underscore': new RegExp('^_'),
    'is StudlyCase': new RegExp('([A-Z][a-z]+)*[A-Z][a-z]*'),
    'is kebab-case': new RegExp('[a-z0-9\\-]+'),
};

const sets = [
    {
        'pattern': 'theme/assets/**/*',
        'rules': ['no underscores', 'no capitals', 'no spaces', 'is kebab-case'],
    },
    {
        'pattern': 'resources/**/*.js',
        'rules': ['no underscores', 'no capitals', 'no spaces'],
    },
    {
        'pattern': 'resources/**/*.vue',
        'rules': ['no underscores', 'no hyphen', 'is StudlyCase', 'no spaces'],
    },
    {
        'pattern': 'resources/**/components/*.scss',
        'rules': ['no spaces', 'has leading underscore', 'no capitals', 'is kebab-case'],
    },
    {
        'pattern': 'resources/**/*.php',
        'rules': ['no spaces', 'no capitals', 'is kebab-case', 'no underscores'],
    },
    {
        'pattern': 'app/*/**.php',
        'rules': ['no spaces', 'is StudlyCase', 'no underscores'],
    },
];

let errors = [];

const processSet = function (set) {
    const files = glob.sync([
        path.join(process.env.PWD, set.pattern),
    ]);

    files.forEach(file => processFile(file, set));
};

const processFile = function (file, set) {
    const name = path.basename(file).replace(/\..+$/, '');

    const ignored = ignoreList.find(path => {
        return file.indexOf(path) !== -1;
    });

    if (ignored) {
        return;
    }

    const fails = set.rules.filter(ruleName => {
        if (ruleName.indexOf('is ') === 0 || ruleName.indexOf('has ') === 0) {
            return !name.match(rules[ruleName]);
        }

        return name.match(rules[ruleName]);
    });

    if (fails.length) {
        return fails.forEach(rule => {
            errors.push({
                'file': file,
                'rule': rule,
            });
        });
    }
};

sets.forEach(processSet);

if (errors.length) {
    console.table(errors);
    process.exit(1);
}

console.log('🎉 Conventions all okay!');
process.exit();
