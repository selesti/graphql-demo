<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>GraphQL</title>
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    </head>
    <body>
        <div id="app">
            <graphql inline-template>
                <div class="container my-4">
                    <div>
                        <h2 class="mb-1">Request</h2>
                        <prism-editor :code="query" v-model="query" language="graphql"></prism-editor>
                    </div>

                    <div class="mt-4">
                        <h2 class="mb-1">Result</h2>
                        <json-viewer :value="preview" :expand-depth="10"></json-viewer>
                    </div>
                </div>
            </graphql>
        </div>
        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
