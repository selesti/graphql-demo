import 'core-js';
import Vue from 'vue';
import 'prismjs';
import 'prismjs/themes/prism.css';
import 'vue-prism-editor/dist/VuePrismEditor.css';
import JsonViewer from 'vue-json-viewer'
import PrismEditor from 'vue-prism-editor'
import Graphql from './components/GraphQL'

require('./bootstrap');

Vue.component('graphql', Graphql);
Vue.component('prism-editor', PrismEditor);
Vue.component('json-viewer', JsonViewer);

if (document.getElementById('app')) {
    new Vue({
        el: '#app',
    })
}
