/*
* You can fetch the data however you want, just make sure
* you return the JSON result inside .data
*/

export default async (query) => {

    // hit the /graphql endpoint
    // with a POST payload that has a "query" param
    // that has your graphql query within it.
    // e.g. PHP would see: $_POST['query'] = "mutation { updateUser(id: 1 name: 'new name') { name } }"

}
