import handler from './demo'

export default {
    data: () => ({
        query: `query {

}`,
        preview: {},
    }),
    watch: {
        query(query) {
            this.playground(query)
        },
    },
    methods: {
        async playground(query) {
            let result = null;

            try {
                result = await handler.call(this, query)
            } catch (e) {
                return console.log(e)
            }

            if (result) {
                this.preview = result
            }
        },
    },
}
